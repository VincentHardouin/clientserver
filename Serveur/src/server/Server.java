package server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Server {

	private final static int PORT = 5656; 
	static List<DataOutputStream> clients = new ArrayList<>(); 

	public static void main(String[] args) {

		System.out.println("Serveur !");

		try(ServerSocket ss = new ServerSocket(PORT,2)) {

			ss.setSoTimeout(20000);

			System.out.println("En attente d'un client");


			while(clients.size() < 1) {
				Socket s = ss.accept();
				DataInputStream dIn = new DataInputStream(s.getInputStream());
				DataOutputStream dOut = new DataOutputStream(s.getOutputStream());
				clients.add(dOut);

				ClientHandler client = new ClientHandler(s,dOut, dIn); 
				Thread t = new Thread(client);
				t.start();
			}


			ss.close();


		} catch (IOException e) {

			e.printStackTrace();
		} finally {

		}



	}


}

class ClientHandler implements Runnable {
	private Socket socket;
	private DataOutputStream dOut; 
	private DataInputStream dIn; 
	private static int connexion = 1;
	private int id;
	private SimpleDateFormat d = new SimpleDateFormat ("dd/MM/yyyy" );
	private SimpleDateFormat h = new SimpleDateFormat ("hh:mm");
	
	public ClientHandler(Socket socket, DataOutputStream dOut, DataInputStream dIn) {
		this.socket = socket;
		this.dOut = dOut; 
		this.dIn = dIn; 
		id = connexion;
		
		connexion++;
		System.out.println("Client n:" + id + " ip : " + socket.getInetAddress());
	}

	@Override
	public void run() {
		try {
			String message = null;
			while(true) {
				Date currentTime = new Date();
				// String date = d.format(currentTime);
				String heure = h.format(currentTime);
				
				message = heure + " id:" + id + " msg: " +dIn.readUTF();
				System.out.println("Transmission : "+message);

				Iterator<DataOutputStream> it = Server.clients.iterator(); 
				while (it.hasNext()) {
					try {
						DataOutputStream dos = it.next();
						dos.writeUTF(message);
						dos.flush();
					}
					catch (Exception e) { }
				}  

			}

		}
		catch (Exception e) { }
	}
}