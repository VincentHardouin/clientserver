package client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String[] args) throws UnknownHostException {
		int serverPort = 5656;
		InetAddress inetAdd = InetAddress.getByName("127.0.0.1");

		try(Socket socket = new Socket(inetAdd, serverPort);) {

			System.out.println("Connexion au serveur établi");


			InputStream in = socket.getInputStream();
			OutputStream out = socket.getOutputStream();

			DataInputStream dIn = new DataInputStream(in);
			DataOutputStream dOut = new DataOutputStream(out);

			GetNewMsg getMsg = new GetNewMsg(dIn);
			Thread t = new Thread(getMsg);
			t.start();


			BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Ecrire un msg :");
			String line = null;
			while ((line = keyboard.readLine()) != null ) {
				if (line.equalsIgnoreCase("bye")) {
					break;
				}
				dOut.writeUTF(line);
				dOut.flush();
				System.out.println("Ecrire un msg :");

			}

			
			getMsg.terminate();
			
			t.interrupt();
			t.join();
			
			
			in.close();
			out.close();
			dOut.close();
			dIn.close();
			keyboard.close();
			
			socket.close();
		}
		catch (Exception e) { }
	}
}


class GetNewMsg implements Runnable {
	DataInputStream dIn;
	boolean running = true;
	GetNewMsg(DataInputStream dIn) {
		this.dIn = dIn;
	}
	public void terminate() {
		running = false;
	}
	@Override
	public void run() {
		String newMsg = null;

		while (running) {
			try {
				newMsg = dIn.readUTF();
				System.out.println("Nouveau message : " + newMsg);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		
		try {
			dIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
